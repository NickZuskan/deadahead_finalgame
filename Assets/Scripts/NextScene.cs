﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour
{
    public int nextSceneToLoad;
    public int activeScene;

    void FixedUpdate()
    {
        nextSceneToLoad = activeScene + 1;
    }

    private void Update()
    {
        activeScene = SceneManager.GetActiveScene().buildIndex;
    }


}
