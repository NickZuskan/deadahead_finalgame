﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTurret : MonoBehaviour
{
    private GameObject target;
    private bool targetLocked;
    public GameObject turret;
    public GameObject arrow;
    public float firingTime;
    private bool shotReady;
    public GameObject arrowSpawnPoint;
    public int fireRate;


    private void Start()
    {
        shotReady = true;
    }

    private void Update()
        //shooting and detecting head
    {
        if (targetLocked)  
        {
            turret.transform.LookAt(target.transform);
            if (shotReady)
            {
                Shoot();
            }
        }
    }

    void Shoot()
    {//arrow being fired and then setting notReady to true
        Transform _arrow = Instantiate(arrow.transform, arrowSpawnPoint.transform.position, Quaternion.identity);
        _arrow.transform.rotation = arrowSpawnPoint.transform.rotation;
        shotReady = false;
        StartCoroutine(FireRate());
    }

    IEnumerator FireRate()
    {// time between shots
        yield return new WaitForSeconds(2);
        shotReady = true;
    }

    private void OnTriggerEnter(Collider other)
    {//target players head
        if (other.tag == "ControllerWithHead")
        {
            target = other.gameObject;
            targetLocked = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ControllerWithHead")
        {
            target = other.gameObject;
            targetLocked = false;
        }
    }
}
