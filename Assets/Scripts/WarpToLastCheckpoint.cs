﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpToLastCheckpoint : MonoBehaviour
{
   

    void OnTriggerEnter(Collider other) //sets the players checkpoint 
    {
        if (other.CompareTag("ControllerWithHead"))
        {
            GameMaster.Instance.WarpToLastCheckpoint();
        }
    }
}
